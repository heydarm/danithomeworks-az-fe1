const ul = document.querySelector('.list');
let arr = [1, 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', ['9', '2', '3', 'sea', 'user', 23], 1, 'world', 'Kiev', 'Kharkiv', 'Odessa']

function printList(arr) {

    const liArr = arr.map(function callback(value){
        if(typeof value == 'object') {
            return value;
        } else {
            return `<li>${value}</li>`;
        }
    });

    for (let li of liArr) {

        if (typeof li == 'object') {

            let ulNew = document.createElement('ul');
            ul.append(ulNew);

            let liInner = li.map((value) => `<li>${value}</li>`);

            for(let li of liInner) {
                ulNew.innerHTML += li;
            }

        } else {
            ul.innerHTML += li;
        }

    }

}

printList(arr);

let counter =  10;
const counterEl = document.querySelector('.counter');

let interval = setInterval(()=> {
    if (counter != 0) {
        counterEl.innerText = (`Time before removing ${--counter}`);
    } else {
        clearInterval(interval);
        document.body.innerHTML = '';
    }
}, 1000);




