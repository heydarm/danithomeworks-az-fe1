let eyeIcons = document.querySelectorAll('.icon-password');
let eyeIcon = document.querySelector('.fa-eye');
let eyeIconSlash = document.querySelector('.fa-eye-slash');
let submitBtn = document.querySelector('.btn');

for (let icon of eyeIcons) {
    icon.addEventListener('click', changeIcon);
}

function changeIcon() {

    let parentEl = this.parentElement;
    let inputEl = parentEl.querySelector('input');

    if (this.classList.contains('fa-eye')) {

        inputEl.type = 'text';
        
        this.classList.remove('fa-eye');
        this.classList.add('fa-eye-slash');
        
    } else {

        inputEl.type = 'password';

        this.classList.remove('fa-eye-slash');
        this.classList.add('fa-eye');

    }

}

submitBtn.addEventListener('click', sumbitHandler);

function sumbitHandler() {

    this.preventDefault;

    let inputPassword = document.querySelector('.password');
    let inputPasswordConfirm = document.querySelector('.password-confirm');
    let msgWrong = document.querySelector('.msg-wrong');

    if (inputPassword.value === inputPasswordConfirm.value) {

        if(msgWrong) {
            msgWrong.remove();
        }
        alert('You are welcome');

    } else {

        if(!msgWrong) {
            inputPasswordConfirm.insertAdjacentHTML('afterend', `<div class="msg-wrong">Нужно ввести одинаковые значения</div>`);
        } else {
            msgWrong.remove();
            inputPasswordConfirm.insertAdjacentHTML('afterend', `<div class="msg-wrong">Нужно ввести одинаковые значения</div>`);
        }

    }

}