let m;

while (!(Number(m))) {
    m = +prompt('Enter the smaller number:');
}

let n;

while (!(Number(n))) {
    n = +prompt('Enter the larger number:');
}

if (m < n) {

    for (let i = m; i < n; i++) {

        let isPrime = true;

        for (let j = 2; j <= i; j++) {

            if (i % j === 0 && j !== i) {
                isPrime = false;
            }

        }

        if (isPrime == true) {
            console.log(i);
        }

    }

} else {
    console.log('You entered wrong numbers. Please, restart the page and try again.');
}