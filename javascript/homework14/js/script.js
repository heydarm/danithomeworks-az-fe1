$(document).ready(function(){

    $('.tabs-title').click(function() {

        let thisTabTitle = $(this);

        $(".active").removeClass("active");
        $(this).addClass("active");

        $(".tabs-item").removeClass("display-none").filter(function() {
            return $(this).attr("data-category") != $(thisTabTitle).attr("data-category");
        }).addClass("display-none");;

    })

});