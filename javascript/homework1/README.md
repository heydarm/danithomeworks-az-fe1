# Explain in your own words the difference between declaring variables via var, let and const.
### The scope of the let variable is within the blocks, and the scope of the variable is within the function. And the const variable cannot be reassigned.

# Why is declaration of a variable via var considered a bad tone?
### Its value will be undefined if you try to access it before its declaration. This can cause problems later.

