let name;
let age;

// Checking that "name" is not a number and isn't empty

do {
    name = prompt('Name (Letters only):', name);
} while (name == "" || Boolean(Number(name)) == true);

// Checking that "age" is not a letters and isn't empty

do {
    age = prompt('Age (Numbers only):', age);
} while (Boolean(Number(age)) == false);

// Age access checking

if (age < 18) {
    alert('You are not allowed to visit this website.');

} else if (age > 22) {
    alert('Welcome, ' + name);

} else if (18 <= age <= 22) {
    
    let isConfirmed = confirm('Are you sure you want to continue?');

    if (isConfirmed == true) {
        alert('Welcome, ' + name);
    }

    if (isConfirmed == false) {
        alert('You are not allowed to visit this website.');
    }

}