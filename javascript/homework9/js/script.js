const tabsTitles = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.tabs-item');

for (let tabTitle of tabsTitles) {
    tabTitle.addEventListener('click', tabsHandler);
}

function tabsHandler() {

    let activeClass = document.querySelector('.active');

    activeClass.classList.remove('active');
    this.classList.add('active');

    for (let tabItem of tabsItems) {

        if (this.dataset.category != tabItem.dataset.category) {
            tabItem.classList.add('display-none');
        } else {
            tabItem.classList.remove('display-none');
        }

    }

}