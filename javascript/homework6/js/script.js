let array = [];
let arrayFiltered = [];
let type = '';
let user = {};
let id = Symbol("id");

function filterBy(array, type) {
    array.forEach(element => {

        if (type == 'string'){
            if (typeof element !== 'string') {
                arrayFiltered.push(element);
            }
        } else if (type == 'number') {
            if (typeof element !== 'number') {
                arrayFiltered.push(element);
            }
        } else if (type == 'boolean') {
            if (typeof element !== 'boolean') {
                arrayFiltered.push(element);
            }
        } else if (type == 'null') {
            if(typeof element !== 'null') {
                arrayFiltered.push(element);
            }
        } else if (type == 'undefined') {
            if(typeof element !== 'undefined') {
                arrayFiltered.push(element);
            }
        } else if (type == 'object') {
            if(typeof element !== 'object') {
                arrayFiltered.push(element);
            }
        } else if (type == 'symbol') {
            if(typeof element !== 'symbol') {
                arrayFiltered.push(element);
            }
        }

    });
}

filterBy(['hello', 'world', 23, '23', null, true, false, 0, '', undefined, user, id], 'object');

console.log(arrayFiltered);