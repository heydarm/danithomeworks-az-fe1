const inputData = document.querySelector('.input');
const pageWrapper = document.querySelector('.input-wrapper');
let removeBtns;

inputData.addEventListener('blur', addPriceSpan);

function addPriceSpan() {

    let value = inputData.value;
    let span = `
    <div class="price-wrapper">
        <span class="price">Current price: ${value}</span>
        <button class="remove-button">x</button>
    </div>
    `;

    if (value >= 0 && value != '') {

        let wrongMsg = document.querySelector('.wrong-msg');
        if (wrongMsg) {
            wrongMsg.remove();
        }

        inputData.style.borderColor = '';
        inputData.insertAdjacentHTML('beforebegin', span);
        inputData.value = '';

    } else if (inputData.style.borderColor != 'red') {

        inputData.style.borderColor = 'red';
        inputData.insertAdjacentHTML('afterend', `<span class="wrong-msg">Please enter correct price</span>`);

    }

    removeBtns = document.querySelectorAll('.remove-button');
    for (let removeBtn of removeBtns) {
        removeBtn.addEventListener('click', removePriceSpan);
    }

}

function removePriceSpan() {
    this.parentElement.remove();
}