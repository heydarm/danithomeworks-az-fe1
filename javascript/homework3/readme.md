# Describe in your own words what the functions in programming are for.
### Functions are needed in order to not use the same code in different places if necessary.

# Describe in your own words what is the reason why arguments are defined in a function. Why are they passed when a function is called?
### Arguments are defined in a function to pass information inside it. They are passed so that the function can operate on them and then returns the values to us.

