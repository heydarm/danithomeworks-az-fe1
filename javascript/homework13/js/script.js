let themeChangeBtn = document.querySelector('.theme-change-btn');
let root = document.documentElement;
let user;
let color = 'blue';
color = localStorage.getItem(user);

let colorsBlue = {
    ['--logo-second-col']: getComputedStyle(root).getPropertyValue('--logo-second-col'),
    ['--main-col-dark']: getComputedStyle(root).getPropertyValue('--main-col-dark'),
    ['--footer-menu-col']: getComputedStyle(root).getPropertyValue('--footer-menu-col'),
    ['--footer-menu-content-col']: getComputedStyle(root).getPropertyValue('--footer-menu-content-col'),
    ['--header-nav-link-hover-bgcol']: getComputedStyle(root).getPropertyValue('--header-nav-link-hover-bgcol'),
    ['--side-menu-text-col']: getComputedStyle(root).getPropertyValue('--side-menu-text-col'),
}

let colorsRed = {
    ['--logo-second-col']: '#FF637D',
    ['--main-col-dark']: '#5C4747',
    ['--footer-menu-col']: '#331715',
    ['--footer-menu-content-col']: '#B34756',
    ['--header-nav-link-hover-bgcol']: '#473231',
    ['--side-menu-text-col']: '#705D5D',
}

if (color == 'blue') {
    for (let item in colorsRed) {
        root.style.setProperty(item, colorsBlue[item]);
    };
}

if (color == 'red') {
    for (let item in colorsBlue) {
        root.style.setProperty(item, colorsRed[item]);
    };
}

themeChangeBtn.addEventListener('click', function() {

    if (color == 'blue') {
        for (let item in colorsRed) {
            root.style.setProperty(item, colorsRed[item]);
        };
        color = 'red';
        localStorage.setItem(user, color);
        return color = 'red';
    }

    if (color == 'red') {
        for (let item in colorsBlue) {
            root.style.setProperty(item, colorsBlue[item]);
        };
        color = 'blue';
        localStorage.setItem(user, color);
        return color = 'blue'; 
    }

})