let time = 10;
let timer = setInterval(timerFunc, 10);

function timerFunc() {

    let timerWrapper = document.querySelector('.timer');

    time -= 0.01
    timerWrapper.innerHTML = `${time.toFixed(2)}`

    if (time <= 0) {
        time = 10;
    }

}

let imgs = document.querySelectorAll('.image-to-show');
let imgNum = 0;
let slideShowInterval = setInterval(slideShow, 10000);

function slideShow() {

    if (document.querySelector('.active')) {
        document.querySelector('.active').classList.remove('active');
    }

    if (imgNum < 4) {

        imgNum++;

        if (imgNum == 4) {
            imgNum = 0;
        }

        imgs[imgNum].classList.add('active');

        return imgNum;

    }

}

let resumeBtn = document.querySelector('.resume');
let stopBtn = document.querySelector('.stop');

stopBtn.addEventListener('click', function() {
    clearInterval(slideShowInterval);
    clearInterval(timer);
    time = 10;
})

resumeBtn.addEventListener('click', function() {
    clearInterval(slideShowInterval);
    clearInterval(timer);
    slideShowInterval = setInterval(slideShow, 10000);
    time = 10;
    timer = setInterval(timerFunc, 10);
})