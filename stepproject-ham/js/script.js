let worksLoadMore = () => {

    const worksLoadMoreBtn = document.querySelector('.load-more-button.works-section');

    let loadData = [
        {
            imgSrc: 'img/graphic-design/graphic-design5.jpg',
            category: 1,
        },
        {
            imgSrc: 'img/graphic-design/graphic-design6.jpg',
            category: 1,
        },
        {
            imgSrc: 'img/graphic-design/graphic-design7.jpg',
            category: 1,
        },
        {
            imgSrc: 'img/web-design/web-design5.jpg',
            category: 2,
        },
        {
            imgSrc: 'img/web-design/web-design6.jpg',
            category: 2,
        },
        {
            imgSrc: 'img/web-design/web-design7.jpg',
            category: 2,
        },
        {
            imgSrc: 'img/landing-page/landing-page5.jpg',
            category: 3,
        },
        {
            imgSrc: 'img/landing-page/landing-page6.jpg',
            category: 3,
        },
        {
            imgSrc: 'img/landing-page/landing-page7.jpg',
            category: 3,
        },
        {
            imgSrc: 'img/wordpress/wordpress5.jpg',
            category: 4,
        },
        {
            imgSrc: 'img/wordpress/wordpress6.jpg',
            category: 4,
        },
        {
            imgSrc: 'img/wordpress/wordpress7.jpg',
            category: 4,
        }
    ]

    worksLoadMoreBtn.addEventListener('click', loadMore);

    function loadMore() {

        const loader = document.querySelector('.loader');
        loader.classList.add('active');

        let loaderDelete = () => loader.classList.remove('active');
        
        
        let items = [];

        for(let item of loadData) {

            function worksTextP2(item) {
    
                if (item.category == 1) {
                    return ('Graphic Design');
                }
    
                if (item.category == 2) {
                    return ('Web Design');
                }
    
                if (item.category == 3) {
                    return ('Landing Page');
                }
    
                if (item.category == 4) {
                    return ('Wordpress');
                }
    
            }

            items.push(`
                <div data-category="${item.category}" class="works-item">
                    <img src="${item.imgSrc}" alt="#">
                    <div class="works-img--hov">
                        <a href="#" class="works-icon link-icon"> <img src="img/link-icon-green.svg" alt="Link"> </a>
                        <a href="#" class="works-icon search-icon"> <img src="img/search-icon-green.svg" alt="Search"> </a>
                        <p class="works-img--hov-p1">CREATIVE DESIGN</p>
                        <p class="works-img--hov-p2">${worksTextP2(item)}</p>
                    </div>
                </div>
            `);

        }
        
        for (let item of items) {
            const worksItemsWrapper = document.querySelector('.works-items-wrapper');
            let a = document.createElement('div');
            a.innerHTML = item;
            
            worksItemsWrapper.append(a)

            document.querySelector('.works').style.height = 'auto';
            worksItemsWrapper.style.height = 'auto';
        }

        worksLoadMoreBtn.style.display = 'none';

        setTimeout(loaderDelete, 2000);

        worksTabsHandler();
    }

}

worksLoadMore();




let servicesTabsHandler = () => {
    const servicesBtnsWrapper = document.querySelector('.services-buttons-wrapper')
    const servicesButtons = servicesBtnsWrapper.querySelectorAll('.services-button');
    const servicesItems = document.querySelectorAll('.services-item');
    let tabName;

    for (let button of servicesButtons) {
        button.addEventListener('click', btnActivator);
    }

    function btnActivator() {
        let activeClass = document.querySelector('.active');
        activeClass.classList.remove("active");
        this.classList.add("active");
        tabName = this.getAttribute('data-tab-name');
        selectTabItem(tabName);
    }

    function selectTabItem(tabName) {
        for (let item of servicesItems) {
            item.classList.contains(tabName) ? item.classList.add('active') : item.classList.remove('active');
        }
    }
}

servicesTabsHandler();




function worksTabsHandler() {

    const worksBtnsWrapper = document.querySelector('.works-buttons-wrapper');
    const worksBtns = worksBtnsWrapper.querySelectorAll('.works-button');
    const worksItems = document.querySelectorAll('.works-item');
    let tabName;

    for (let button of worksBtns) {
        button.addEventListener('click', btnActivator);
    }

    function btnActivator() {
        let activeClass = worksBtnsWrapper.querySelector('.active');
        activeClass.classList.remove('active');
        this.classList.add('active');
        tabName = this.getAttribute('data-category');
        selectTabItem(tabName);
    }

    function selectTabItem(tabName) {

        if (!(tabName == 0)) {

            for(let item of worksItems) {
                let dataCategory = item.getAttribute('data-category');

                item.classList.remove('non-active');

                if (dataCategory != tabName) {
                    item.classList.add('non-active');
                }
            }

        } else if (tabName == 0) {
            for(let item of worksItems) {
                item.classList.remove('non-active');
            }
        }

    }

}

worksTabsHandler();